# Website

This website is an ongoing project to demonstrate that I can actually deploy a real "thing"
on the internet. It's really nothing fancy and more of a "lab environment" than anything
else. Most of the updates I do here are around the infrastruture / devops kind. One day I
plan to fill it with some more technical write-ups. I will try to keep the projects section
updated as best I can with whatever I have worked on that I feel is "done" enough to share
publically. There are many other projects I've started that are not in a finished state and
won't be listed here, although you may find them on my personal github or gitlab pages.

## How to run
To run locally, set `ENV=dev` when running. You can see and modify how this works in the `init()` func.

If no options are specified, this will assume you are trying to run on the production server. This means you will need to have the server cert and key, as well as all the correct files and folders in the right places.

## Roadmap
- [x] Rewrite the main website to use Go as the backend [Issue](https://gitlab.com/wgraham800/website/-/issues/4)
- [x] Rewrite the CI/CD pipeline to build/deploy and use the Go binary [Issue](https://gitlab.com/wgraham800/website/-/issues/7)
- [x] Add https support [Issue](https://gitlab.com/wgraham800/website/-/issues/5)
- [x] Periodically pull & sync markdown files from a remote repository [Issue](https://gitlab.com/wgraham800/website/-/issues/8)
- [x] Periodically pull & sync images from an aws s3 bucket [Issue](https://gitlab.com/wgraham800/website/-/issues/14)
- [x] Add some badges [Issue](https://gitlab.com/wgraham800/website/-/issues/9)
- [x] Mature the CI/CD pipeline [Issue](https://gitlab.com/wgraham800/website/-/issues/6)

## Authors and acknowledgment
[Will Graham](https://gitlab.com/wgraham800)  

## License
All content in this repository is Copyright (c) 2024 Will Graham, except where otherwise noted.

## Project status
This project is always in development
