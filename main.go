package main

import (
	"errors"
	"html/template"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var baseURL string = "wgraham.io/"
var protocol string = "https://"
var notesURL, AKnotesURL string
var tpl *template.Template
var notes []string
var debug bool = false

var routes struct {
	BaseURL string
}

var (
	httpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"method", "endpoint"},
	)
)

func init() {
	env := strings.ToLower(os.Getenv("ENV"))
	if env == "dev" {
		debug = true
		baseURL = "mysite.com/"
		protocol = "http://"
	}

	notesURL = baseURL + "static/notes_html/"
	routes.BaseURL = protocol + baseURL

	notes = compileMarkdownNotes("md_notes/", "static/notes_html/")
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))

	prometheus.MustRegister(httpRequestsTotal)
}

func main() {
	fs := http.FileServer(http.Dir("static"))

	// Prometheus metrics exporting
	http.Handle("/metrics", promhttp.Handler())

	// AJW routes
	http.Handle(baseURL+"imgs/", fs)
	http.Handle(baseURL+"css/", fs)
	http.HandleFunc(baseURL, willIndex)
	http.HandleFunc(baseURL+"projects", projectHandler)
	http.HandleFunc(baseURL+"contact", contactHandler)
	http.HandleFunc(baseURL+"notes", notesIndex)
	http.HandleFunc(notesURL, notesHandler)

	// Default routes
	http.Handle(baseURL+"favicon.ico", http.NotFoundHandler())

	// Handle debug vs prod server
	if debug {
		log.Print("Starting up the local debug server")
		err := http.ListenAndServe(":80", nil)
		if err != nil {
			log.Println("Something went wrong just after starting the server")
			log.Fatalln(err)
		}
	} else {
		// Redirect HTTP to HTTPS
		go func() {
			if err := http.ListenAndServe(":80", http.HandlerFunc(redirectToTls)); err != nil {
				log.Fatalf("ListenAndServe error: %v", err)
			}
		}()

		log.Print("Starting up the production server")
		err := http.ListenAndServeTLS(":443", "cert/cert.pem", "cert/key.pem", nil)
		if err != nil {
			log.Println("Something went wrong just after starting the server")
			log.Fatalln(err)
		}
	}
}

func redirectToTls(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, "https://"+req.Host+req.RequestURI, http.StatusMovedPermanently)

}

// AJW handlerfuncs
func willIndex(w http.ResponseWriter, req *http.Request) {
	tpl.ExecuteTemplate(w, "willHeader.gohtml", struct {
		Page string
	}{
		Page: "home",
	})
	tpl.ExecuteTemplate(w, "willHome.gohtml", nil)
	tpl.ExecuteTemplate(w, "willFooter.gohtml", nil)

	httpRequestsTotal.WithLabelValues(req.Method, req.URL.Path).Inc()
}

func projectHandler(w http.ResponseWriter, req *http.Request) {
	tpl.ExecuteTemplate(w, "willHeader.gohtml", struct {
		Page string
	}{
		Page: "projects",
	})
	tpl.ExecuteTemplate(w, "willProjects.gohtml", nil)
	tpl.ExecuteTemplate(w, "willFooter.gohtml", nil)

	httpRequestsTotal.WithLabelValues(req.Method, req.URL.Path).Inc()
}

func contactHandler(w http.ResponseWriter, req *http.Request) {
	tpl.ExecuteTemplate(w, "willHeader.gohtml", struct {
		Page string
	}{
		Page: "contact",
	})
	tpl.ExecuteTemplate(w, "willContact.gohtml", nil)
	tpl.ExecuteTemplate(w, "willFooter.gohtml", nil)

	httpRequestsTotal.WithLabelValues(req.Method, req.URL.Path).Inc()
}

func notesIndex(w http.ResponseWriter, req *http.Request) {
	tpl.ExecuteTemplate(w, "willHeader.gohtml", struct {
		Page string
	}{
		Page: "notes",
	})
	tpl.ExecuteTemplate(w, "noteIndex.gohtml", notes)
	tpl.ExecuteTemplate(w, "willFooter.gohtml", nil)
	httpRequestsTotal.WithLabelValues(req.Method, req.URL.Path).Inc()
}

func notesHandler(w http.ResponseWriter, req *http.Request) {
	note := req.URL.Path[1:]
	_, err := os.Open(note)
	if err != nil {
		log.Print(errors.New("Something went wrong while trying to read the " + note + " file"))
		log.Print(errors.New("this is likely a request for a resource that doesn't exist"))
		http.Redirect(w, req, "/notes", http.StatusMovedPermanently)
		return
	}

	tpl.ExecuteTemplate(w, "willHeader.gohtml", nil)
	// w.Header().Set("Content-Type", "text/html")
	s := "<div class=\"row text-start \">"
	w.Write([]byte(s))
	http.ServeFile(w, req, note)
	s = "</div>"
	w.Write([]byte(s))
	tpl.ExecuteTemplate(w, "willFooter.gohtml", nil)
	httpRequestsTotal.WithLabelValues(req.Method, req.URL.Path).Inc()
}

