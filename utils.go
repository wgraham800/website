package main

import (
	"log"
	"os"

	"github.com/gomarkdown/markdown"
)

// Translates markdown notes from source dir to html files in target dir
func compileMarkdownNotes(source string, target string) []string {
	flist, err := os.ReadDir(source)
	if err != nil {
		log.Println("Somthing went wrong while parsing the", source, "folder.")
		log.Fatalln(err)
	}

	// Read all file contents and convert the markdown to html
	// and write the html out to a new file
	files := make([][]byte, len(flist))
	notes := make([]string, len(flist))
	var finame, foname string

	for i := 0; i < len(flist); i++ {
		finame = flist[i].Name()
		notes[i] = finame[:len(finame)-3]
		files[i], err = os.ReadFile(source + finame)
		if err != nil {
			log.Println("Somthing went wrong while reading the", finame, "file.")
			log.Fatalln(err)
		}

		files[i] = markdown.ToHTML(files[i], nil, nil)

		foname = target + finame[:len(finame)-3] + ".html"
		err := os.WriteFile(foname, files[i], 0666)
		if err != nil {
			log.Println("Somthing went wrong while writing the", foname, "file out as HTML.")
			log.Fatalln(err)
		}
	}
	return notes
}
